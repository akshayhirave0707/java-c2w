import java.io.*;

class Demo{
	public static void main(String [] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows:");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row ; i++) {
			int n = (row-i+1)*i;
			for(int j = 1 ; j<= row -i+1 ;j++) {
				System.out.print(n +" ");
				n= n-i;
			}
			System.out.println();
		}
	}
}
