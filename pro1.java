import java.io.*;

class Demo{
	public static void main(String [] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows:");
		int row = Integer.parseInt(br.readLine());
		
		for(int i = 1; i<=row ; i++) {
			char ch = 64;
			int n;
			if(i%2==1) {
				ch+=row;
				n=row;
			}else {
				n = 1;
				ch+=1;
			}
			for(int j =1;j<=row;j++) {
				System.out.print(ch+"" + n + " ");

				if(i%2==1) {
					ch--;
					n--;
				}else {
					ch++;
					n++;
				}
				
			}
			System.out.println();
		}	
	}


}

