import java.io.*;

class Demo{
	public static void main(String [] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the character 1:");
		char ch1 = (char) br.read();
		br.skip(1);
		System.out.println("Enter the character 2:");
		char ch2 = (char) br.read();

		if(ch1==ch2) {
			System.out.println("The characters are "+ch1);
		}else {
			if(ch1>90)
				ch1-=32;
			if(ch2>90)
				ch2-=32;
			int diff;
			if(ch1>ch2)
				diff = ch1-ch2;
			else 
				diff = ch2-ch1;
			System.out.println("The difference between "+ch1 +" & "+ch2+" is "+diff);
		}
	}
}

		

