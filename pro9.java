import java.io.*;

class Demo {
	public static void main(String [] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the integer:");
		
		int n = Integer.parseInt(br.readLine());
		int temp = n;
		
		int sum = 0;

		while(n!=0) {
			int f =1;
			for(int i = n%10; i>1 ; i--) {
				f*=i;
			}	
			sum+=f;
			n/=10;
		}		
		System.out.println("The Addition of factorials of each digit from "+temp+" = "+sum);
	
	}


}

