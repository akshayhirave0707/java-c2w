import java.io.*;

class Demo {
	public static void main(String [] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter the start:");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter the end:");
		int end = Integer.parseInt(br.readLine());

		while(start<=end) {
			for(int i = 2 ; i<= start ;i++) {
				if(i==start)
					System.out.print(i+" ");
				if(start%i==0)
					break;
			}
			start++;
		}
		System.out.println();
		
	
	}


}

