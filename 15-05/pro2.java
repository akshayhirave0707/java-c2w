import java.io.*;

class StringDemo {

	static int checkPalindromeString(String str) {
		char [] ch = str.toCharArray();

		int l = 0,r = ch.length -1;

		while(l<r) {
			if(ch[l++] != ch[r--])
				return 0;
	
		}
		return 1;
		


	}

	int checkAnagramStrings(String str1, String str2) {
		char [] ch1= str1.toCharArray();
		char [] ch2 = str2.toCharArray();
		if(ch1.length != ch2.length)
			return 0;

		int [] arr1 = new int[123];
		int [] arr2 = new int[123];

		for(int i = 0;i<ch1.length;i++) {
			arr1[ch1[i]]++;
			arr2[ch2[i]]++;
		}
		for(int i = 65;i<123;i++) {
			if(arr1[i] != arr2[i])
				return 0;
			if(i==90)
				i+=6;
		}
		return 1;
	}
	public static void main(String [] args) throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the string to check palindrome or not:");
		String str = br.readLine();
		if(checkPalindromeString(str)==1)
			System.out.println("The String "+str+" is palindrome String");
		else
			System.out.println("The String "+str+" is not palindrome String");
		
		System.out.println("Enter the First String:");
		String str1 = br.readLine();

		System.out.println("Enter the Second String:");
		String str2 = br.readLine();
		
		StringDemo obj = new StringDemo();

		if(obj.checkAnagramStrings(str1,str2) ==1)
			System.out.println("The two strings are anagram Strings");
		else
			System.out.println("The Two strings are not anagram Strings");


	
	}


}

