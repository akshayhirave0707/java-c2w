import java.io.*;

class Demo{
	public static void main(String [] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number of rows:");
		int row = Integer.parseInt(br.readLine());
		
		int n = row *(row+1)/2;
		char ch = 64;
		ch+=n;

		for(int i = 1; i<= row; i++) {
			for(int j = 1;j<=i; j++) {
				if(row%2==1) {
					if(i%2==1)
						System.out.print(ch +" ");
					else
						System.out.print(n +" ");
				}else {
					if(i%2==1)
						System.out.print(n+" ");
					else
						System.out.print(ch+" ");
				}
				ch--;
				n--;
			}
			System.out.println();
		}
	}
}
